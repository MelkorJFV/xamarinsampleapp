﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinSampleApp
{
    public class Core
    {
        public static async Task<Weather> GetWeather (string location)
        {
            //location = "?id=3435910"
            string endpoint = "http://api.openweathermap.org/";
            string append = "&APPID=351b0c79cd9e7d53da8b3d823e12584e";
            
            string weather_Append = "data/2.5/weather";

            string query = endpoint + weather_Append + location + append;

            dynamic res = await RestConnector.get(query).ConfigureAwait(false);

            if (res["weather"] != null)
            {
                Weather weather = new Weather();
                

                weather.Temperature = (string)res["main"]["temp"] + " K";
                weather.Humidity = (string)res["main"]["humidity"] + " %";
                weather.Pressure = (string)res["main"]["pressure"]; 
                weather.TempMin = (string)res["main"]["temp_min"];
                weather.TempMax = (string)res["main"]["temp_max"];

                weather.RegionCode = (string)res["name"];

                weather.Country = (string)res["sys"]["country"];

                weather.Longitude = (string)res["coord"]["lon"];
                weather.Latitude = (string)res["coord"]["lat"];

                return weather;
            }else
            {
                return null;
            }
        }
    }
}
