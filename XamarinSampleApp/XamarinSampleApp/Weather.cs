﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XamarinSampleApp
{
    public class Weather
    {
        public string Temperature { get; set; }
        public string Humidity { get; set; }
        public string Pressure { get; set; }
        public string TempMin { get; set; }
        public string TempMax { get; set; }

        public string RegionCode { get; set; }

        public string Country { get; set; }

        public string Longitude { get; set; }
        public string Latitude { get; set; }

        public Weather()
        {
            this.Temperature = "";
            this.Humidity = "";
            this.Pressure = "";
            this.TempMin = "";
            this.TempMax = "";

            this.RegionCode = "";
            this.Country = "";

            this.Longitude = "";
            this.Latitude = "";
                
        }
    }
}
