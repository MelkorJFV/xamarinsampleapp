﻿
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace XamarinSampleApp
{
    class RestConnector
    {

        public static async Task<dynamic> get(string query)
        {
            HttpClient client = new HttpClient();
            var response = await client.GetAsync(query);

            dynamic data = null;
            if (response != null)
            {
                string json = response.Content.ReadAsStringAsync().Result;
                data = JsonConvert.DeserializeObject(json);
            }

            return data;
        }
    }
}
