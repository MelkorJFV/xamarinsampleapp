﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using XamarinSampleApp;

namespace XamarinSampleApp.Droid
{
	[Activity (Label = "XamarinSampleApp.Droid", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : Activity
	{

        string geo = "";

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
            SetContentView (Resource.Layout.Main);

            getData();

            FindViewById<Button>(Resource.Id.mapBtn).Click += gotoMap;
            FindViewById<Button>(Resource.Id.detailsBtn).Click += gotoDetails;

        }

        private async void getData()
        {
            Weather weather = await Core.GetWeather("?id=3435910");
            
            FindViewById<TextView>(Resource.Id.humidity).Text = weather.Humidity;
            FindViewById<TextView>(Resource.Id.countryCode).Text = weather.Country;
            FindViewById<TextView>(Resource.Id.max).Text = weather.TempMax;
            FindViewById<TextView>(Resource.Id.min).Text = weather.TempMin;
            FindViewById<TextView>(Resource.Id.pressure).Text = weather.Pressure;
            FindViewById<TextView>(Resource.Id.regionCode).Text = weather.RegionCode;
            FindViewById<TextView>(Resource.Id.temperature).Text = weather.Temperature;

            geo = "geo:" + weather.Latitude + "," + weather.Longitude;

        }

        private void gotoMap(object sender, EventArgs e)
        {
            try
            {
                var geoUri = Android.Net.Uri.Parse(geo);
                Console.WriteLine(geoUri.ToString());
                var mapIntent = new Intent(Intent.ActionView, geoUri);
                StartActivity(mapIntent);
            }catch(Exception exeption)
            {
                Console.WriteLine(exeption.GetBaseException());       
            }
        }

        private void gotoDetails(object sender, EventArgs e)
        {
            try
            {
                StartActivity(typeof(DetailsActivity));
            }
            catch (Exception exeption)
            {
                Console.WriteLine(exeption.GetBaseException());
            }
        }
    }
}


