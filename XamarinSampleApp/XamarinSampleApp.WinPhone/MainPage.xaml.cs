﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace XamarinSampleApp.WinPhone
{
    public sealed partial class MainPage : Page
    {

        string loc;
        string name;


        public MainPage()
        {
            
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;

            GetWeather();
        }

        private async void GetWeather()
        {

            Weather weather = await Core.GetWeather("?id=3435910");

            countryCode.Text = weather.Country;
            regionCode.Text = weather.RegionCode;


            tempText.Text = weather.Temperature;
            pressure.Text = weather.Pressure;
            humidityText.Text = weather.Humidity;
            minTemp.Text = weather.TempMin;
            maxTemp.Text = weather.TempMax;


            loc = string.Format("{0},{1}", weather.Latitude, weather.Longitude);
            name = weather.RegionCode.Replace("&", "and");

        }

        private void gotoMap(object sender, RoutedEventArgs e)
        {
            var request = string.Format("bingmaps:?cp={0}&q={1}", loc, name);

        }

        private void gotoDetails(object sender, RoutedEventArgs e)
        {

        }
    }
}
