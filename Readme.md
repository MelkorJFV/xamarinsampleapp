**Xamarin**
============================
En el siguiente documento se mostrara el paso a paso para armar una aplicación en Xamarin.

[TOC]


TODO
--------------------------------------------------------------------
 - Code Clean Up
 - Add IOS Sample
 - Improve RestConnector & add POST module
 - Add Details page with custom tables/rows
 - Evolve Sample


Aplicacion de ejemplo
--------------------------------------------------------------------
La aplicacion de ejemplo se conectara a los servicios de [OpenWeatherMaps](https://openweathermap.org/) via Get, y recuperara el Json. Con los datos recuperados poblaremos la pantalla.

Requerimientos Previos
--------------------------------------------------------------------
Para empezar, y como un basico necesitamos bajar [Xamarin](https://www.xamarin.com/download). Al momento de descargar podemos elegir con o sin VS comunity como tambien una version para MacOS, llamada Xamarin Studio.


Creando la Solucion
--------------------------------------------------------------------
Seleccionar dentro de ***Templates -> Visual C# -> Cross-Platform -> Blank App (Native Portable)***
<!-- img1 -->

Llamaremos nuestra aplicacion *"XamarinSampleApp"* por que eso es lo que es.

La nueva solucion consta con los siguientes 4 proyectos:

 - ***XamarinSampleApp (Portable)*** - En esta seccion tendremos todo lo que sea logica de negocio o funcionalidad core de la aplicacion que sea replicable y reutilizable en las 3 plataformas, por ejemplo la coneccion a servicios REST (como haremos a continuacion)
 - ***XamarinSampleApp.Droid*** - En este proyecto tendremos las cosas puntuales y unicas pertenecientes a Android, como el diseño de UI
 - ***XamarinSampleApp.IOS*** - Idem a la perteneciente a Android, pero orientada a IOS. Nota especial, si se encuentra trabajando desde un Windows, No podra editar o abrir el storyboard para poder diseñar las pantallas, como tampoco podra ejecutar la aplicacion, al no ser que se conecte a una Mac, proveyendo de la IP de la misma al Xamarin Studio (VS)
 - ***XamarinSampleApp.WinPhone (Windows Phone 8.1)*** - Idem a la perteneciente a IOS. En este caso la aclaracion es hacia el otro lado, si ud se encuentra trabajando en una Mac, devera proveer al Xamarin Studio de la IP de un windows, en caso contrario no podra editar los xaml, como tampoco podra ejecutar el emulador.


**Motivos de las restricciones**

Android trabaja sobre Java, por lo cual tanto sus emuladores o cosas propietarias se pueden ejecutar sobre cualquier plataforma. Vale hacer notar que Xamarin instalara su propio ADV, por lo cual utilizara sus propias VM. Se recomienda utilizar las mismas para hacer las pruebas de desarrollo.

IOS y Windows Phone no son emuladores, son lo que se denomina simuladores, los mismos utilizan el core del OS (MacOs o Windows respectivamente) para poder ejecutar el mismo requiere poder acceder a dicho core, es por eso que al levantar el proyecto en un Windows, nos pedira la IP de una Mac o viceversa.

Esto no es solo aplicable al momento de emular, sino tambien para poder hacer el render del Storyboard de IOS desde Windows, el mismo se grafica utilizando el core de Mac, por lo cual no podermos siquiera hacer el diseño grafico de la pantalla de IOS desde un Windows si no contamos con una Mac en el dominio.

Finalmente el firmado de la aplicacion para distribucion debe ser hecho con las herramientas correspondientes a la plataforma. En el caso de Android, solo se requiere Java, en el caso de IOS, se requiere una Mac con un XCode registrado, y para WinPhone un Visual Studio con las firmas registradas.

Esto puede parecer un contratiempo y algo que lleva para atras la idea de multiplataforma, pero es una restriccion que existe tanto en Ionic como en QT5, aunque sea solo al momento de firmar, son restricciones de las plataformas las cuales no podemos esquivar.


Comenzando el desarrollo
--------------------------------------

Dado que en este ejemplo estaremos trabajando con [Json](http://www.json.org/) la primer cosa que debemos hacer es agregar, desde NuGet alguna libreria para manejo del mismo.
En el Solution Explorer, sobre la solucion "*Solution 'XamarinSampleApp' (4 projects)*" boton derecho y seleccionar *"Manage NuGet packages for solution..."*  Buscamos e instalamos la ultima version de "*Newtonsoft.Json*"

**Portable** - *Generico para todos*
La primer cosa que debemos hacer es conectarnos al servicio REST y recuperar el Json correspondiente. Creamos una nueva clase, dentro del proyecto *Portable*, la cual llamaremos "RestConnector"
El codigo de la misma se encuentra en este repositorio, pueden ver su funcionamiento, pero nos enfocaremos en la conexion, la misma se hace de la siguiente manera:

```cs
class RestConnector
{
	/*En este caso el parametro query utilizado en la invocacion es la URL ya formateada con todos sus params requeridos para recuperar los datos*/ 
	public static async Task<dynamic> get(string query)
        {
	    //Creamos un cliente Http
            HttpClient client = new HttpClient();
            //hacemos, de forma asincronica el pedido al servicio
            var response = await client.GetAsync(query);
            dynamic data = null;
            if (response != null)
            {
	        //Recuperamos el resultado de la respuesta
                string json = response.Content.ReadAsStringAsync().Result;
                /*utilizando la libreria de Newtonsoft pasamos el string a un objeto Json*/
                data = JsonConvert.DeserializeObject(json);
            }
            //devolvemos el mapa dinamico generado
            return data;
        }
    }
```

Como siguiente paso tambien dentro de *Portable* generamos una nueva clase, *Weather* en este caso, sera el objeto el cual contendra los datos requeridos. Dado que es una clase sencilla y no requiere logica de ningun tipo, mirar en el repositorio por la misma.

Como siguiente paso invocaremos al RestConnector e interpretaremos el Json, instanciando nuestro objeto Weather.
Esto lo haremos desde la clase ya existente llamada *Core* la misma sera la que usaremos como puente entre las implementaciones puntuales de cada plataforma y lo que sea generico.
 
 Llamamos al RestConnector y recuperamos en una variable de tipo dynamic el resultado, el cual esperamos sea el mapa representativo del Json.
```cs
dynamic res = await RestConnector.get(query).ConfigureAwait(false);
/*sabemos que nuestro Json tiene un nodo que se llama "weather" asi que nos aseguramos que el mismo figure*/
if (res["weather"] != null)
            {
                Weather weather = new Weather();
                weather.Temperature = (string)res["main"]["temp"] + " K";
                weather.Humidity = (string)res["main"]["humidity"] + " %";
                weather.Pressure = (string)res["main"]["pressure"]; 
                //Y asi con los datos que querramos
                //Devolvemos (al codigo de cada UI) el objeto Weather
                return weather;
            }
```


**UI e integracion con la seccion de Portable**
---------------------------------------------------------------------

**Android**

Ahora, para la edicion de la UI, vamos primero con *Android* por lo cual si vamos al proyecto *"XamarinSampleApp.Droid"* podemos ver que las estructuras de carpetas de recursos se respeta como si fuese un proyecto nativo. 
Dado que salimos de un proyecto template, podemos encontrar ya layout basico, el cual modificaremos. El mismo se encuentra en  **Resources -> Layout ->Main.axml** 
La diferencia a una aplicacion nativa es que aqui estaremos usando *axml* en lugar del *xml* estandad de Android. En si, es lo mismo.
Reemplazamos todo lo existente por lo del ejemplo (o por esta seccion en particular para tener un unico campo y un boton)

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".Activity.MainActivity"
    android:orientation="vertical">
    <TextView
        android:layout_width="match_parent"
        android:layout_height="0dp"
        android:layout_weight="1"
        android:text="Temperature" />
    <TextView
        android:id="@+id/temperature"
        android:layout_width="match_parent"
        android:layout_height="0dp"
        android:layout_weight="1" />
    <Button
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="Map"
        android:id="@+id/mapBtn"
        android:layout_gravity="center" />
</LinearLayout>
```

Tambien podemos hacer el diseño de UI por drag and drop de componentes, pero no recomiendo esto.

Como siguiente paso, debemos recuperar la informacion, utilizando el Core del Portable como puente y mostrar la informacion en el/los campos
Para eso, podemos ver que existe dentro del proyecto *.Droid* el archivo *MainActivity.cs*, generado automaticamente.
Agregamos el siguiente metodo:


```cs
private async void getData()
{	
	/*Llamamos al Core y recuperamos nuestro objeto Weather*/
	Weather weather = await Core.GetWeather("?id=3435910");
        //Buscamos el view por Id al que le querramos asignar el texto y lo hacemos.
        FindViewById<TextView>(Resource.Id.temperature).Text = weather.Temperature;
}
```

Finalmente, todo lo que debemos hacer es llamar este metodo desde algun lado que sepamos la aplicacion va a pasar de forma mandatoria.
En el metodo *onCreate*  agregamos la invocacion a este metodo:

```cs
protected override void OnCreate (Bundle bundle)
{
	base.OnCreate (bundle);
	SetContentView (Resource.Layout.Main);
            
	getData();
}
```


**Windows Phone**

Pasemos ahora nuestra atencion al proyecto *XamarinSampleApp.WinPhone*, para aquellos que hayan trabajado previamente con WP 8.1 en adelante, esta parte es innecesaria, ya que se hace igual. Entendamos que Xamarin, por mas que sea una solucion hibrida, cuando se trata del mundo Microsoft, sigue siendo nativo.

Podemos ver, dentro de este proyecto, que se genero un template, *MainPage.xaml*. En el mismo encontraremos el diseño de pantalla, y si en Solution Explorer exapndimos el mismo encontraremos es .cs asociado.

Reemplazamos el contenido del .xaml, dentro de tag de Page, por lo siguiente y tendremos solamente un campo de texto donde colocaremos la temperatura, y un boton.

```xml
<StackPanel>
	<TextBlock HorizontalAlignment="Left" FontSize="14" Text="Temperature" />
	<TextBlock x:Name="tempText" Margin="10,0,0,10" HorizontalAlignment="Left" FontSize="18" TextWrapping="Wrap" VerticalAlignment="Top"/>

	<Button HorizontalAlignment="Center" FontSize="14" Background="Gray" Click="gotoMap">
		<TextBlock Text="MAP" Foreground="Black" FontSize="14"/>
	</Button>
</StackPanel>
```

Tambien podemos hacer el diseño de UI por drag and drop de componentes, pero no recomiendo esto.

Como siguiente paso debemos llamar al servicio del Core, recuperar nuestro elemento Weather y completar los campos con los datos recuperados del servicio, para esto abrimos el .cs asociado al xaml.

En el mismo agregamos el siguiente metodo:
```cs
private async void GetWeather()
{
	//Llamamos al GetWeather del Core
	Weather weather = await Core.GetWeather("?id=3435910");
	//Asignamos la temperatura al TextBlock tempText
	tempText.Text = weather.Temperature;
}
```

Al ya estar relacionado el xaml con el cs no es necesario buscar el TextBlock, como requiere ser hecho en Android, el *name* que asignamos desde el xaml puede ser usado como una variable en el .cs

Hecho esto, y dado que el metodo esta declarado como async, lo podemos invocar desde algun lugar que forzosamente el codigo pase, al igual que en Android que existe el metodo *OnCreate* donde agregamos la invocacion al nuevo metodo, aqui lo haremos desde la constructora del objeto, donde, simplemente agregaremos la invocacion, quedando de la siguiente forma:

```cs
public MainPage()
{
	this.InitializeComponent();
	this.NavigationCacheMode = NavigationCacheMode.Required;
	//Agregamos el llamado a nuestro nuevo metodo
	GetWeather();
}
``` 

Con esto tenemos finalizada nuestra aplicacion, basica que se conectara al servicio y nos mostrara la temperatura.


**Fin**
---------------------------------------------------------------------------
Seleccionando el proyecto de inicio, ya sea .Droid o .WinPhone podremos ejecutar en emulador, dispositivo fisico o buildear la solucion para ser distribuida.